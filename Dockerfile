FROM python:3.6-alpine

RUN apk add --no-cache \
    gcc \
    git \
    musl-dev

RUN git clone git://github.com/osrg/ryu.git /root/ryu
RUN cd /root/ryu \
 && pip install .

RUN pip install pyyaml toml

ARG CTRL_BASE=/controllers
COPY ./controllers ${CTRL_BASE}

COPY entrypoint.sh /root/entrypoint.sh
RUN chmod +x /root/entrypoint.sh

ENV CONTROLLER hub.py
WORKDIR ${CTRL_BASE}
ENTRYPOINT [ "sh", "-c", "/root/entrypoint.sh"]