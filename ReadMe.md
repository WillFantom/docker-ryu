# Docker Ryu

This has been created mainly for use in SCC365.

## Building

This builds ryu from the master src branch by default.

```bash
docker build --rm --compress -t ryu:latest .
```

## Running

You can run this by mounting a volume containing your controllers and giving the file name using the CONTROLLER env var.

```bash
docker run --rm -it -p 6653:6653 -v $(pwd)/controllers:/controllers -e CONTROLLER=myswitch.py willfantom/ryu:latest
```
