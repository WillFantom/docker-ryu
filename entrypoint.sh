#!/bin/sh
set -e

if [ -f requirements.txt ]; then pip install -r requirements.txt; fi
ryu-manager --verbose $CONTROLLER

exit
